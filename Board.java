
public class Board {
	private Square[][] tictactoeBoard;

	public Board() {
		this.tictactoeBoard = new Square[3][3];
		
		for (int row = 0; row < this.tictactoeBoard.length; row++) {
			for (int col = 0; col < this.tictactoeBoard[row].length; col++) {
				
				this.tictactoeBoard[row][col] = Square.BLANK;
			}
		}
		
	}
	public String toString() {
		
		String boardRepresentation="";
		boardRepresentation+="  0 1 2\n";
		for (int row = 0; row < this.tictactoeBoard.length; row++) {
			boardRepresentation+=row+" ";
			
			for (int col = 0; col < this.tictactoeBoard[row].length; col++) {
				
				boardRepresentation+=this.tictactoeBoard[row][col]+" ";
			}
			boardRepresentation+="\n";
		}
		
		return boardRepresentation;
	}
	public boolean placeToken(int row, int col, Square playerToken) {
		
		if(row>=3 || col >=3) {
			return false;
		}
		
		if(this.tictactoeBoard[row][col]==Square.BLANK) {
			this.tictactoeBoard[row][col]=playerToken;
			return true;
		}else {
			return false;
		}
	}
	public boolean checkIfFull() {
		
		boolean checker=true;
		for (int row = 0; row < this.tictactoeBoard.length; row++) {
			for (int col = 0; col < this.tictactoeBoard[row].length; col++) {
				
				if(this.tictactoeBoard[row][col] == Square.BLANK) {
					checker=false;
				}
			}
		}
		return checker;
	}
	private boolean checkIfWinningHorizontal(Square playerToken) {
		
		boolean horizontalWin=false;
		for(int row=0;row<this.tictactoeBoard.length;row++) {
			if(this.tictactoeBoard[row][0]==playerToken && this.tictactoeBoard[row][1]==playerToken && this.tictactoeBoard[row][2]==playerToken ) {
				horizontalWin=true;
			}
			
		}
		return horizontalWin;
	}
	private boolean checkIfWinningVertical(Square playerToken) {
		boolean verticalWin=false;
		int firstCol=0;
		int secondCol=0;
		int thirdCol=0;
		for(int row=0;row<this.tictactoeBoard.length;row++) {
			if(this.tictactoeBoard[row][0]==playerToken) {
				firstCol++;
			}
			if(this.tictactoeBoard[row][1]==playerToken) {
				secondCol++;
			}
			if(this.tictactoeBoard[row][2]==playerToken) {
				thirdCol++;
			}
			
		}
		if(firstCol==3 || secondCol==3 || thirdCol==3 ) {
			verticalWin=true;
		}
		return verticalWin;
	}
	private boolean checkIfWinningDiagonal(Square playerToken) {
		boolean diagonalWin=false;
		int leftDiagonal=0;
		int rightDiagonal=0;
		for(int row=0;row<this.tictactoeBoard.length;row++) {
			
			if(this.tictactoeBoard[row][0+row]==playerToken) {
				leftDiagonal++;
			}
			if(this.tictactoeBoard[row][2-row]==playerToken) {
				rightDiagonal++;
			}
		}
		if(leftDiagonal==3 || rightDiagonal==3) {
			diagonalWin=true;
		}
		
		return diagonalWin;
	}
	public boolean checkIfWinning(Square playerToken) {
		boolean win=false;
		
		if(checkIfWinningHorizontal(playerToken)) {
			win=true;
		}
		
		if(checkIfWinningVertical(playerToken)) {
			win=true;
		}
		
		if(checkIfWinningDiagonal(playerToken)) {
			win=true;
		}
		
		return win;
	}
}
