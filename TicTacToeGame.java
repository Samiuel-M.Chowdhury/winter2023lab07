import java.util.Scanner;
public class TicTacToeGame {
	public static void main(String[] args) {
		Scanner keyboard=new Scanner(System.in);
		System.out.println("Welcome to tic-tac-toe");
		Board boardGame=new Board();
		
		boolean gameOver=false;
		int player=1;
		Square playerToken=Square.X;
		
		while(!gameOver) {
			System.out.println(boardGame);
			if(player==1) {
				playerToken=Square.X;
			}else {
				playerToken=Square.O;
			}
			
			System.out.println("Player "+player+" ,choose a row:");
			int playRow=keyboard.nextInt();
			System.out.println("Choose a column:");
			int playCol=keyboard.nextInt();
			
			boolean placingAToken=boardGame.placeToken(playRow,playCol,playerToken);
			if(placingAToken==false) {
				do {
					System.out.println("You have chosen an invalid spot. Enter a new set of coordinates"+"\nChoose a row:");
					playRow=keyboard.nextInt();
					System.out.println("Choose a column:");
					playCol=keyboard.nextInt();
					placingAToken=boardGame.placeToken(playRow,playCol,playerToken);
				}while(placingAToken==false);
			}
			
			if(boardGame.checkIfFull()==true) {
				System.out.println("Its a tie!");
				gameOver=true;
			}else if(boardGame.checkIfWinning(playerToken)==true){
				System.out.println("Player "+player+" won the game!");
				gameOver=true;
			}else {
				player+=1;
				if(player>2) {
					player=1;
				}
			}
		}
	}
}
